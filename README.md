# Finzerb

Un projet django pour faire des playlists spotify à partir des dates d'ajout des 
morceaux dans sa bibliothèque ou la date de sortie des disques.

## Comment ça marche

1. Aller sur https://finzerb.canarduck.com
2. Se connecter à spotify
3. Choisir les playlists dans lesquelles chercher les morceaux et les dates
4. Créer une nouvelle playlist avec les morceaux

## Développement

### Dépendances

- Python >= 3.12
- Django 5
- Spotipy
- gunicorn

+ d'autres (cf. pyproject.toml)

### Environnement

Un `Makefile` permet de lancer les commandes principales pour instancier un environnement
de développement, faire un `make help` pour voir le détail.