"""Processeurs de contexte."""
from typing import TYPE_CHECKING

from core.helpers import get_client, validate_token

if TYPE_CHECKING:
    from django.http.request import HttpRequest


def auth(request: "HttpRequest") -> bool:
    """Balise pour savoir si l'utilisateur/rice a une session spotify valide."""
    spotify = get_client(request)
    return {"is_authenticated": validate_token(spotify)}
