"""Formulaires."""
from itertools import batched
from typing import TYPE_CHECKING
from urllib.parse import parse_qsl, urlparse

from django import forms
from unidecode import unidecode

from .helpers import get_client

if TYPE_CHECKING:
    from django.http.request import HttpRequest


class AuthForm(forms.Form):
    """Authentification spotify."""

    def __init__(self, url: str, *args, **kwargs) -> None:
        """Transformation de l'url d'auth spotify en champs de formulaire."""
        super().__init__(*args, **kwargs)
        fields = parse_qsl(urlparse(url).query)
        for key, value in fields:
            self.fields[key] = forms.CharField(
                widget=forms.HiddenInput(), initial=value
            )


class SetupForm(forms.Form):
    """Paramétrage."""

    playlists = forms.MultipleChoiceField(
        label="Dans les playlists", widget=forms.CheckboxSelectMultiple
    )
    date_added = forms.CharField(
        label="Filtrer par date d'ajout",
        max_length=10,
        widget=forms.TextInput(
            attrs={"placeholder": "AAAA-MM-JJ, tout est facultatif"}
        ),
        required=False,
    )
    date_released = forms.CharField(
        label="Filtrer par date de sortie",
        max_length=10,
        widget=forms.TextInput(
            attrs={"placeholder": "AAAA-MM-JJ, tout est facultatif"}
        ),
        required=False,
    )

    def __init__(self, playlists: list[dict[str, str]], *args, **kwargs) -> None:
        """Passage des playlists en paramètre."""
        super().__init__(*args, **kwargs)
        saved = [("saved", "Titres likés ♥")]
        other_playlists = [(p["id"], p["name"]) for p in playlists]
        # tri par nom en ignorant casse & accents
        choices = saved + sorted(other_playlists, key=lambda p: unidecode(p[1].lower()))
        self.fields["playlists"].choices = choices

    def save(self, request: "HttpRequest") -> None:
        """Enregistrement des choix dans la session."""
        request.session["date_added"] = self.cleaned_data["date_added"]
        request.session["date_released"] = self.cleaned_data["date_released"]
        request.session["playlists"] = self.cleaned_data["playlists"]


class CreatePlaylistForm(forms.Form):
    """Créer une playlist."""

    tracks = forms.MultipleChoiceField(
        label="Pistes", widget=forms.CheckboxSelectMultiple
    )
    name = forms.CharField(label="Nom de la playlist", max_length=100)
    public = forms.BooleanField(label="Playlist publique", required=False)

    def __init__(self, entries: list[dict[str, str]], *args, **kwargs) -> None:
        """Passage des morceaux en paramètre."""
        super().__init__(*args, **kwargs)
        choices: list[tuple[str, str]] = []
        tracks_ids: set[str] = set()
        for entry in entries:
            track = entry["track"]
            track_id = track["id"]
            if track_id in tracks_ids:
                continue
            tracks_ids.add(track_id)
            choices.append(
                (
                    track_id,
                    f'{track["artists"][0]["name"]} - {track["name"]}',
                )
            )
        self.fields["tracks"].choices = choices
        self.fields["tracks"].initial = [c[0] for c in choices]

    def save(self, request: "HttpRequest") -> None:
        """Création de la playlist privée avec les morceaux sélectionnés."""
        spotify = get_client(request)
        playlist = spotify.user_playlist_create(
            request.session["spotify_user_id"],
            self.cleaned_data["name"],
            public=self.cleaned_data["public"],
        )
        for tracks in batched(self.cleaned_data["tracks"], 100):
            spotify.playlist_add_items(playlist["id"], tracks)
