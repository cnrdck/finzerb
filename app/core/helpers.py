"""Boite à outils."""
from collections.abc import Callable
from functools import wraps
from typing import TYPE_CHECKING, TypeVar

import spotipy
from django.conf import settings
from django.contrib import messages
from django.core.cache import cache
from django.shortcuts import redirect
from django.urls import reverse
from typing_extensions import ParamSpec

if TYPE_CHECKING:
    from django.http.request import HttpRequest

P = ParamSpec("P")
T = TypeVar("T")


CACHE_KEYS = {
    "playlists": "playlists_{user_id}",
    "playlist": "playlist_{playlist_id}",
    "saved": "saved_{user_id}",
}


def validate_token(spotify: spotipy.Spotify) -> bool:
    """Validation des jetons spotify."""
    return spotify.auth_manager.validate_token(
        spotify.auth_manager.cache_handler.get_cached_token()
    )


def token_required(func: Callable[P, T]) -> Callable[P, T]:
    """Décorateur de vues qui nécessitent une authentification Spotify."""

    @wraps(func)
    def wrapper(request: "HttpRequest", *args, **kwargs) -> T:
        """Vérification du jeton."""
        spotify = get_client(request)
        if not validate_token(spotify):
            messages.warning(request, "Session Spotify expirée.")
            return redirect(reverse("home"))
        return func(request, *args, **kwargs)

    return wrapper


def get_client(request: "HttpRequest") -> spotipy.Spotify:
    """Cache session spotipy et OAuth."""
    cache_handler = spotipy.DjangoSessionCacheHandler(request)
    auth_manager = spotipy.oauth2.SpotifyOAuth(
        client_id=settings.SPOTIPY_CLIENT_ID,
        client_secret=settings.SPOTIPY_CLIENT_SECRET,
        redirect_uri=request.build_absolute_uri(reverse("callback")),
        scope=",".join(settings.FINZERB_SCOPES),
        cache_handler=cache_handler,
        show_dialog=True,
    )
    return spotipy.Spotify(auth_manager=auth_manager)


def get_playlists(request: "HttpRequest") -> list[dict]:
    """Récupération des playlists depuis le cache (ou non)."""
    spotify = get_client(request)
    user_id = request.session["spotify_user_id"]
    cache_key = CACHE_KEYS["playlists"].format(user_id=user_id)
    if playlists := cache.get(cache_key):
        return playlists
    results = spotify.current_user_playlists()
    playlists = results["items"]
    while results["next"]:
        results = spotify.next(results)
        playlists.extend(results["items"])
    cache.set(cache_key, playlists, tag=user_id)  # FIXME TTL
    return playlists


def get_tracks(request: "HttpRequest") -> list[dict]:
    """Récupération des morceaux des playlists depuis le cache (ou non)."""
    spotify = get_client(request)
    user_id = request.session["spotify_user_id"]
    entries = []  # la liste des morceaux de toutes les playlists sélectionnées
    for playlist_id in request.session["playlists"]:
        cache_key = (
            CACHE_KEYS["saved"].format(user_id=user_id)
            if playlist_id == "saved"
            else CACHE_KEYS["playlist"].format(playlist_id=playlist_id)
        )
        # On cache les items d'une playlist (ou des likes)
        if items := cache.get(cache_key):
            entries.extend(items)
            continue
        # Si le cache n'existe par on va chercher les items
        results = (
            spotify.current_user_saved_tracks()
            if playlist_id == "saved"
            else spotify.playlist_items(playlist_id)
        )
        playlist_entries = results["items"]
        while results["next"]:
            results = spotify.next(results)
            playlist_entries.extend(results["items"])
        cache.set(cache_key, playlist_entries)  # FIXME TTL
        entries.extend(playlist_entries)
    return entries
