"""Vues."""
from typing import TYPE_CHECKING

import spotipy
from django.contrib import messages
from django.http import HttpResponse, HttpResponseBadRequest
from django.shortcuts import redirect, render
from django.urls import reverse

from .forms import AuthForm, CreatePlaylistForm, SetupForm
from .helpers import get_client, get_playlists, get_tracks, token_required

if TYPE_CHECKING:
    from django.http.request import HttpRequest


def home(request: "HttpRequest") -> HttpResponse:
    """Accueil."""
    spotify = get_client(request)
    form = AuthForm(spotify.auth_manager.get_authorize_url())
    return render(
        request,
        "home.html",
        {
            "form": form,
            "form_action": spotipy.oauth2.SpotifyOAuth.OAUTH_AUTHORIZE_URL,
        },
    )


def callback(request: "HttpRequest") -> HttpResponse:
    """Traitement du code d’authentification spotify."""
    spotify = get_client(request)
    code = request.GET.get("code")
    if not code:
        return HttpResponseBadRequest("Code de réponse spotify manquant.")
    try:
        spotify.auth_manager.get_access_token(code)
    except Exception as exc:
        messages.error(request, f"Échec de l'authentification spotify: {exc}")
        return redirect(reverse("home"))
    request.session["spotify_user_id"] = spotify.me()["id"]
    messages.success(request, "Authentification spotify réussie.")
    return redirect(reverse("form"))


def logout(request: "HttpRequest") -> HttpResponse:
    """Déconnexion."""
    request.session.flush()
    messages.info(request, "Ayé, déconnecté.")
    return redirect(reverse("home"))


@token_required
def form(request: "HttpRequest") -> HttpResponse:
    """Formulaire."""
    playlists = get_playlists(request)
    if request.method == "POST":
        form = SetupForm(playlists=playlists, data=request.POST)
        if form.is_valid():
            form.save(request)
            return redirect(reverse("playlist"))
    else:
        initial = {
            "playlists": request.session.get("playlists", ["saved"]),
            "date_added": request.session.get("date_added"),
            "date_released": request.session.get("date_released"),
        }
        form = SetupForm(playlists=playlists, initial=initial)

    return render(request, "form.html", {"form": form})


@token_required
def playlist(request: "HttpRequest") -> HttpResponse:
    """Aperçu des titres."""
    entries = get_tracks(request)
    selected_entries = entries
    if date_added := request.session["date_added"]:
        selected_entries = [
            e for e in selected_entries if e["added_at"].startswith(date_added)
        ]
    if date_released := request.session["date_released"]:
        selected_entries = [
            e
            for e in entries
            if e["track"]["album"]["release_date"].startswith(date_released)
        ]

    if request.method == "POST":
        form = CreatePlaylistForm(entries=selected_entries, data=request.POST)
        if form.is_valid():
            form.save(request)
            messages.success(
                request, f"La playlist {form.cleaned_data['name']} a été créée."
            )
            return redirect(reverse("home"))
    else:
        form = CreatePlaylistForm(entries=selected_entries)

    return render(request, "playlist.html", {"form": form})
