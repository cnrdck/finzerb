"""Paramètres Django."""
from pathlib import Path

from django.core.management.utils import get_random_secret_key
from environs import Env

APP_DIR = Path(__file__).resolve().parent.parent
BASE_DIR = APP_DIR.parent

env = Env()
env.read_env()

SECRET_KEY = env.str("DJANGO_SECRET_KEY", get_random_secret_key())
DEBUG = env.bool("DJANGO_DEBUG", False)

ALLOWED_HOSTS = env.list("ALLOWED_HOSTS", ["finzerb.localhost"])
INSTALLED_APPS = [
    "django.contrib.sessions",
    "django.contrib.messages",
    "django.contrib.staticfiles",
    "core",
]
MIDDLEWARE = [
    "django.middleware.security.SecurityMiddleware",
    "django.contrib.sessions.middleware.SessionMiddleware",
    "django.middleware.common.CommonMiddleware",
    "django.middleware.csrf.CsrfViewMiddleware",
    "django.contrib.messages.middleware.MessageMiddleware",
    "django.middleware.clickjacking.XFrameOptionsMiddleware",
]
ROOT_URLCONF = "finzerb.urls"
TEMPLATES = [
    {
        "BACKEND": "django.template.backends.django.DjangoTemplates",
        "DIRS": [],
        "APP_DIRS": True,
        "OPTIONS": {
            "context_processors": [
                "django.template.context_processors.debug",
                "django.template.context_processors.request",
                "django.contrib.messages.context_processors.messages",
                "core.context_processors.auth",
            ],
            "builtins": ["django.templatetags.static"],
        },
    },
]

WSGI_APPLICATION = "finzerb.wsgi.application"

LANGUAGE_CODE = "fr-fr"
TIME_ZONE = "Europe/Paris"
USE_I18N = True
USE_TZ = True

STATIC_URL = "static/"
STATIC_ROOT = BASE_DIR / "static"

SESSION_ENGINE = "django.contrib.sessions.backends.signed_cookies"
SESSION_COOKIE_HTTPONLY = True
SESSION_COOKIE_SECURE = True

CSRF_COOKIE_SECURE = True

SECURE_SSL_REDIRECT = env.bool("SECURE_SSL_REDIRECT", True)

CACHES = {
    "default": {
        "BACKEND": "diskcache.DjangoCache",
        "LOCATION": BASE_DIR / "cache",
    },
}

# Paramètres finzerb
SPOTIPY_CLIENT_ID = env.str("SPOTIPY_CLIENT_ID")
SPOTIPY_CLIENT_SECRET = env.str("SPOTIPY_CLIENT_SECRET")
FINZERB_SCOPES = [
    "user-library-read",
    "playlist-read-private",
    "playlist-modify-private",
]
