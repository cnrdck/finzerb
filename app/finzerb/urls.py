"""Routage."""
from core import views
from django.urls import path

urlpatterns = [
    path("", views.home, name="home"),
    path("callback", views.callback, name="callback"),
    path("formulaire", views.form, name="form"),
    path("playlist", views.playlist, name="playlist"),
    path("logout", views.logout, name="logout"),
]
