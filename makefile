info: header
define HEADER
   ▄████████  ▄█  ███▄▄▄▄    ▄███████▄     ▄████████    ▄████████ ▀█████████▄  
  ███    ███ ███  ███▀▀▀██▄ ██▀     ▄██   ███    ███   ███    ███   ███    ███ 
  ███    █▀  ███▌ ███   ███       ▄███▀   ███    █▀    ███    ███   ███    ███ 
 ▄███▄▄▄     ███▌ ███   ███  ▀█▀▄███▀▄▄  ▄███▄▄▄      ▄███▄▄▄▄██▀  ▄███▄▄▄██▀  
▀▀███▀▀▀     ███▌ ███   ███   ▄███▀   ▀ ▀▀███▀▀▀     ▀▀███▀▀▀▀▀   ▀▀███▀▀▀██▄  
  ███        ███  ███   ███ ▄███▀         ███    █▄  ▀███████████   ███    ██▄ 
  ███        ███  ███   ███ ███▄     ▄█   ███    ███   ███    ███   ███    ███ 
  ███        █▀    ▀█   █▀   ▀████████▀   ██████████   ███    ███ ▄█████████▀  
                                                       ███    ███

endef
export HEADER

dev: ## Lancer le serveur de dev (django runserver)
	python app/manage.py runserver

run: ## Lancer le serveur de prod (gunicorn)
	python -m gunicorn --chdir app finzerb.wsgi

requirements: ## Maj des dependances puis syncho de l'environnement
	python -m piptools compile --upgrade --resolver backtracking --verbose \
		--strip-extras --output-file=requirements.txt pyproject.toml
	python -m piptools compile --upgrade --resolver backtracking --verbose \
		--strip-extras --extra=dev --output-file=requirements.dev.txt pyproject.toml
	python -m piptools sync requirements.txt requirements.dev.txt

shell: ## shell django
	python app/manage.py shell

manage: ## manage.py + arguments passés avec args, ex. args="makemigrations xxx"
	python app/manage.py $(args)

.PHONY: all

help:
	@echo "$$HEADER"
	@grep -E '^[a-zA-Z_-]+:.*?## .*$$' $(MAKEFILE_LIST) | sort | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}'

.DEFAULT_GOAL := help